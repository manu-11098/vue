# Filtros

## Uso

Vue permite difinir filtros que se pueden utilizar para aplicar formato de texto. Se pueden utilizar tanto en interpolaciones mustache como en expresiones v-bind.

```Vue.js
    <!-- en mustaches -->
    {{ message | capitalize }}

    <!-- en v-bind -->
    <div :id=" rawId | formatId "></div>
```

## Definicion

Se podría definir como un objeto que contiene métodos que reciben un parámetro para que este sea formateado.

Se pueden definir filtros locales dentro de un componente:

```Vue.js
    filters:{
        capitalize (data) {

            if(!data) return '';

            data = data.toString();
            return data.charAt(0).toUpperCase() + value.slice(1);
        }
    }
```

O se pueden definir globalmente antes de crear la instancia de Vue:

```Vue.js
    Vue.filter ('capitalize', function (data) {
        if(!data) return '';

            data = data.toString();
            return data.charAt(0).toUpperCase() + value.slice(1);
    });

    new Vue({
        // ...
    })
```

Si queremos utilizar más de un filtro solamente tenemos que añadirlos a continuación del anterior con otra 'pipe'.

```Vue.js
    <!-- en mustaches -->
    {{ message | capitalize | reverse }}

    <!-- en v-bind -->
    <div :id=" rawId | formatId | capitalize"></div>
```
