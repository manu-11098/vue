# Componentes

## Definición

Los componentes son elementos html reusables y configurables, y nos permiten definir etiquetas que podemos usar en nuestro markup.

Son componibles, de tal modo que se pueden añadir componentes dentro de componenetes y tienen un orden gerárquico, los padres pueden modificar los datos de los hijos pero no al revés.

## Tipos de componentes

### Globales

Pueden ser usados en la misma plantilla en la que está creada la instancia Vue.

```Vue.js
    Vue.component('component-name',{/*...*/})
```

### Locales

Pueden ser enlazados a otros componentes a diferencia de los globales, pero estos han de estar definidos en la instancia Vue.

```Vue.js
var ComponentName = { /* ... */ };
var ComponentName1 = { /* ... */ };

new Vue ({
    el: "#app",
    components: {
        'component-name': ComponentName, /* Nomenclatura kebab-case */
        ComponentName1, /* Nomenclatura PascalCase */
    }
})
```

## Comunicacion entre componentes

### Comunicacion directa padre-hijo

En este modelo solo es posible la comunicacion entre padres e hijos mediante `props` y eventos personalizados.
Se requiren 3 pasos para enviar una propiedad:

1. Registrar la propiedad en el hijo: `props:['name']`

2. Usar la propiedad registrada en el hijo: `<span v-text="name"><span>`

3. Vincular la propiedad variable en el padre cuando llamas al hijo: `<child :name="name"\>`

#### Validando props

```Vue.js
props: {
    // Simple
    name: String,
    // Compleja
    name: {
        type: String,
        default: 'Sample text',
        required: true
    }
}
```

### Comunicacion directa hijo-padre

Tal vez necesitemos cambiar desde uno de los hijos el valor de una propiedad del padre y para eso tenemos que hacer uso de los custom events. Para que los cambios se vean reflejados tenemos que hacer uso de los custom events.

1. En el hijo vamos a emitir un evento  describiendo el cambio que queremos hacer `this.$emit('updatingName', 'NewName')`

2. En el padre necesitamos un detector de eventos. `@updatingName="updateName(newName)`

3. Cuando el evento sea emitido, el metodo asignado actualizara el prop. `this.name = newName`

```Vue.js
Vue.component('Padre',{
  template:`
    <div id="padre">
      <h2>Padre A</h2>
      <pre>data {{ this.$data }}</pre>
      <button @click="reRender">Rerender Padre</button>
      <hr/>
      <hijo :name="name" @updatingName="updateName(newName)"/>  // 2.Registering
    </div>`,
  data() {
    return {
      name: 'name'
    }
  },
  methods: {
    reRender() {
      this.$forceUpdate()
    },
    updateName(newName) {
      this.name = newName  // 3.Updating
    }
  }
})
<!-- -->
Vue.component('Hijo',{
  template:`
    <div id="hijo">
      <h2>Hijo</h2>
      <pre>data {{ this.$data }}</pre>
      <hr/>
      <button @click="changeName">Change Name</button>
      <span>Name: {{ name }}</span>
    </div>`,
  props: ["name"],
  methods: {
    changeName() {
      this.$emit('updatingName', 'newName')  // 1. Emitting
    }
  }
})

```

#### Modificador sync

Vue ofrece el modificador .sync que funciona de forma similar a los eventos y se puede utilizar como atajo. De esta forma en el componente padre no habria ningun metodo.

```Vue.js
Vue.component('Padre',{
  template:`
    <div id="padre">
      <h2>Padre A</h2>
      <pre>data {{ this.$data }}</pre>
      <button @click="reRender">Rerender Padre</button>
      <hr/>
      <hijo :name.sync="name" />  // 2.Registering
    </div>`,
  data() {
    return {
      name: 'name'
    }
  },
  methods: {
    reRender() {
      this.$forceUpdate()
    }
  }
})
<!-- -->
Vue.component('Hijo',{
  template:`
    <div id="hijo">
      <h2>Hijo</h2>
      <pre>data {{ this.$data }}</pre>
      <hr/>
      <button @click="changeName">Change Name</button>
      <span>Name: {{ name }}</span>
    </div>`,
  props: ["name"],
  methods: {
    changeName() {
      this.$emit('update:name', 'newName')  // 1. Emitting
    }
  }
})

```

### Comunicacion multicomponente

Este modelo de comunicacion nos permite que podamos intercambiar datos entre todos los componentes sin necesidad de pasos intermedios ni componentes intermediarios.

#### Bus de evento global

Basicamente es una instancia de Vue que nos permite emitir y detectar eventos como si fuera un bus de datos.

```Vue.js
const eventBus = new Vue() // 1. Declaring

Vue.component('Padre',{
  template:`
    <div id="padre">
      <h2>Padre A</h2>
      <pre>data {{ this.$data }}</pre>
      <button @click="reRender">Rerender Padre</button>
      <hr/>
      <hijo :name.sync="name" />  // 2.Registering
    </div>`,
  data() {
    return {
      name: 'name'
    }
  },created (){
      eventBus.$on('updatingName', this.updateName()) // 3. Listening
  },
  methods: {
    reRender() {
      this.$forceUpdate()
    },
    updateName(newName){
        this.name = newName;
    }
  }
})
<!-- -->
Vue.component('Hijo',{
  template:`
    <div id="hijo">
      <h2>Hijo</h2>
      <pre>data {{ this.$data }}</pre>
      <hr/>
      <button @click="changeName">Change Name</button>
      <span>Name: {{ name }}</span>
    </div>`,
  props: ["name"],
  methods: {
    changeName() {
      eventBus.$emit('update:name', 'newName')  // 2. Emitting
    }
  }
})

```

Tambin podriamos mandar el metodo como una funcion anonima: `created (){ eventBus.$on('updatingName', newName => {this.name = newName})
  }`
