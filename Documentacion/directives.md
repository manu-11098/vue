# Directivas

## Definición

Una directiva es un atributo de un elemento HTML que posteriormente va a ser interpretado por el framework Vue.

### v-text

Te permite incluir el String de la propiedad sin usar la interpolación. Cabe destacar que no renderiza las etiquetas HTML.

```Vue.js

    template: `<h1>{{ title }}</h1> <!-- Con interpolación -->
                <h1 v-text="title"></h1> <!-- Usando la directiva -->
            `,
    data() {
        return {
            title:'Directivas de Vue.js'
        }
    },
```

### v-bind

Te permite añadir propiedades a los atributos de las etiquetas HTML. Para usarlo no es necesario poner `**v-vind:**` puedes utilizar solo los dos puntos como en el ejemplo.

```Vue.js

    template: `<a :href="link.href" :title="link.title" v-text="link.text"></a>`,

    data() {
        return {
            link: {
                text: 'VueJs Home',
                href: 'https://vuejs.org/',
                title: 'Documentacion de VueJs'
            }
        }
    },
```

### v-html

Funciona igual que la directiva v-text, pero esta directiva si que renderiza las etiquetas html. Por lo tanto en el siguiente ejemplo veríamos el `_message_` en negrita.

```Vue.js

    template: `<p v-html="message" ></p>`,

    data() {
        return {
            title:'Directiva HTML',
            message: '<b>Hola desde DirectiveHTML</b>',
        }
    },

}
```

### v-show

Muestra u oculta los elementos añadiendo el atributo css `display:`.

```Vue.js

    template: `<p v-show="show" v-html="message"></p>`,

    data() {
        return {
            show: false, // Cuando es false no se muestra, cuando es true se muestra el <p>
            message: '<b>Hola desde DirectiveHTML</b>',
        }
    },

}
```

### v-if

Evalua una condición al igual que un `if` en cualquier otro lenguaje. Se utiliza igual que el show pero este no modifica elementos, los añade o elimina. Es mucho más seguro utilizar `v-if` siempre que sean datos sensibles.

```Vue.js

    template: `<p v-if="user.premium" v-html="message"></p>`,

    data() {
        return {
            message: '<b>Hola desde DirectiveHTML</b>',
            user: {
                userName: defaultName,
                premium: false,
            }
        }
    },

}
```

También existen `v-if-else` y `v-else`.

```Vue.js

    template:`<p v-if="user.premium && user.vip"> VIP y PREMIUM </p>
                <p v-else-if="user.vip"> Solo VIP </p>
                <p v-else-if="user.premium"> Solo Premium </p>
                <p v-else > Ni VIP ni Premium </p>
            `,
    data() {
        return {
            user: {
                userName: defaultName,
                premium: true,
                vip: true,
            }
        }
    },

```
